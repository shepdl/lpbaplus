package edu.ucla.hcnow.motives

import org.junit.runner.RunWith
import org.specs2.mutable._
import org.specs2.runner._

import scalax.collection.GraphEdge.UnDiEdge
import scalax.collection.edge.WUnDiEdge


/**
 * Created by daveshepard on 10/22/15.
 */
@RunWith(classOf[JUnitRunner])
class UpdateCommunityListSpec extends Specification with GraphTestCase3 {

  val instance = new BipartiteCommunityList[MockNode, WUnDiEdge](graph3, red, blue)

  /*
"The Update Community List function " >> {

  "return the correct value for case 3" >> {



    val newCommunityList = instance.updateCommunityList(communityList, blue)(new CommunityList[MockNode](), )


    newCommunityList(communityList, red, blue).communitiesToNodes(1) must contain(n3(1))
    newCommunityList.communitiesToNodes(1) must contain(n3(5))
    newCommunityList.communitiesToNodes(2) must contain(n3(7))
    newCommunityList.communitiesToNodes(2) must contain(n3(8))
    newCommunityList.communitiesToNodes(3) must contain(n3(11))
    newCommunityList.communitiesToNodes(3) must contain(n3(9))
    newCommunityList.communitiesToNodes(4) must contain(n3(18))
    newCommunityList.communitiesToNodes(4) must contain(n3(15))
    newCommunityList.communitiesToNodes(4) must contain(n3(16))

  }
  }
  */

}
