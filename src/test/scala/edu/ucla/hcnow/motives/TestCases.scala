package edu.ucla.hcnow.motives

import scalax.collection.Graph
import scalax.collection.GraphEdge.UnDiEdge
import scalax.collection.GraphPredef._
import scalax.collection.edge.Implicits._

/**
 * Created by daveshepard on 10/8/15.
 */
trait GraphLabels {

  val red = "red"
  val blue = "blue"
  val tolerance = 0.05

}

trait GraphTestCase1 extends GraphLabels {

  val n = List(null, red, red, blue, blue, red, blue, red, red).zipWithIndex.foldLeft(Map[Int, MockNode]())({
    case (acc, (label, index)) => acc.updated(index, MockNode(index, label))
  })
  val graph = Graph(
    n(1) ~ n(3) % 1, n(1) ~ n(4) % 1, n(2) ~ n(4) % 1, n(5) ~ n(4) % 1, n(4) ~ n(7) % 1, n(7) ~ n(6) % 1, n(6) ~ n(8) % 1
  )
  val communities = new CommunityList[MockNode]()
    .updated(n(1), 1).updated(n(2), 1).updated(n(3), 1).updated(n(4), 1).updated(n(5), 1)
    .updated(n(6), 2).updated(n(7), 2).updated(n(8), 2)

}

trait GraphTestCase2 extends GraphLabels {


  val n2 = List(null, red, red, blue, red, red, red, blue, red, blue, red, blue, red).zipWithIndex.foldLeft(Map[Int, MockNode]()) {
    case (acc, (label, index)) => acc.updated(index, MockNode(index, label))
  }

  val graph2 = Graph(
    n2(1)~n2(3) % 1, n2(2)~n2(3) % 1, n2(3)~n2(4) % 1, n2(3)~n2(5) % 1, n2(5)~n2(7) % 1,
    n2(7)~n2(6) % 1, n2(5)~n2(9) % 1, n2(8)~n2(9) % 1, n2(9)~n2(10) % 1, n2(9)~n2(12) % 1,
    n2(10)~n2(11) % 1, n2(11)~n2(12) % 1
  )

  val communities2 = new CommunityList[MockNode]()
    .updated(n2(1), 1).updated(n2(2), 1).updated(n2(3), 1).updated(n2(4), 1)
    .updated(n2(5), 2).updated(n2(6), 2).updated(n2(7), 2)
    .updated(n2(8), 3).updated(n2(9), 3).updated(n2(10), 3).updated(n2(11), 3).updated(n2(12), 3)

}

trait GraphTestCase3 extends GraphLabels {

  implicit def int2MockNode(i:Int):MockNode = MockNode(i, red)
  implicit def intList2MockNode(is:List[Int]):List[MockNode] = is.map(MockNode(_, red))

  val n3 = List(null, red, red, blue, blue, red, blue, red, red, red, blue,
                red, blue, blue, blue, red, red, blue, red, blue, red,
                blue, blue
  ).zipWithIndex.foldLeft(Map[Int, MockNode]())({
    case (acc, (label, index)) => acc.updated(index, MockNode(index, label))
  })

  val graph3 = Graph(
    n3(1)~n3(3) % 1, n3(1)~n3(4) % 1,
    n3(2)~n3(4) % 1, n3(2)~n3(21) % 1, n3(2)~n3(22) % 1,
    n3(4)~n3(5) % 1, n3(4)~n3(7) % 1,
    n3(6)~n3(7) % 1, n3(6)~n3(8) % 1,
    n3(7)~n3(13) % 1,
    n3(8)~n3(12) % 1, n3(8)~n3(14) % 1,
    n3(9)~n3(10) % 1, n3(9)~n3(12) % 1,
    n3(10)~n3(11) % 1,
    n3(11)~n3(12) % 1,
    n3(13)~n3(15) % 1, n3(13)~n3(16) % 1, n3(13)~n3(18) % 1,
    n3(14)~n3(15) % 1, n3(14)~n3(18) % 1,
    n3(15)~n3(19) % 1, n3(15)~n3(17) % 1,
    n3(16)~n3(17) % 1,
    n3(17)~n3(18) % 1,
    n3(20)~n3(21) % 1, n3(20)~n3(22) % 1
  )

  val communityList = new CommunityList[MockNode](Map[Int, List[MockNode]](
    1 -> List(1, 2, 3, 4, 5),
    2 -> List(6, 7, 8),
    3 -> List(9, 10, 11, 12),
    4 -> List(13, 14, 15, 16, 17, 18, 19),
    5 -> List(20, 21, 22)
  ))

}