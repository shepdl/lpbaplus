package edu.ucla.hcnow.motives

import org.junit.runner.RunWith
import org.specs2.mutable._
import org.specs2.runner._

import scalax.collection.Graph
import scalax.collection.Graph
import scalax.collection.edge.Implicits._
import scalax.collection.GraphEdge.UnDiEdge
import scalax.collection.GraphPredef._
import scalax.collection.edge.WUnDiEdge


/**
 * Created by daveshepard on 10/2/15.
 */
@RunWith(classOf[JUnitRunner])
class BipartiteModularityForGraphSpec extends Specification with GraphTestCase1 with GraphTestCase2 {

  "The Bipartite Modularity For Graph function should" >> {

    "be more or less equal to 0.42857142857142855 for the first graph test case" >> {

      val instance = new BipartiteCommunityList[MockNode, WUnDiEdge](graph, red, blue)
      val modularity = instance.bipartiteModularityForGraph(graph, communities)
      modularity must beCloseTo(0.42857142857142855, tolerance)
    }

    "be more or less equal to 0.5833333333 for the second test case" >> {


      val instance = new BipartiteCommunityList[MockNode, WUnDiEdge](graph2, red, blue)
      val modularity = instance.bipartiteModularityForGraph(graph2, communities)
      modularity must beCloseTo(0.375, tolerance)
    }
  }

}
