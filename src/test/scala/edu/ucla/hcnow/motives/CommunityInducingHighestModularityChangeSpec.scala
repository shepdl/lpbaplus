package edu.ucla.hcnow.motives

import org.junit.runner.RunWith
import org.specs2.mutable._
import org.specs2.runner._

import scalax.collection.GraphEdge.UnDiEdge
import scalax.collection.edge.WUnDiEdge


/**
 * Created by daveshepard on 10/11/15.
 */
@RunWith(classOf[JUnitRunner])
class CommunityInducingHighestModularityChangeSpec extends Specification with GraphTestCase3 {

  val instance = new BipartiteCommunityList[MockNode, WUnDiEdge](graph3, red, blue)

  "The Highest Modularity function" >> {

    "return community with the highest modularity when there are five communities" >> {

      instance.communityInducingHighestModularityChange(graph3, communityList, 1) must_== 4
      instance.communityInducingHighestModularityChange(graph3, communityList, 2) must_== 5
      instance.communityInducingHighestModularityChange(graph3, communityList, 3) must_== 4
      instance.communityInducingHighestModularityChange(graph3, communityList, 4) must_== 1
      instance.communityInducingHighestModularityChange(graph3, communityList, 5) must_== 4

    }

  }

}
