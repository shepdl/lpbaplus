package edu.ucla.hcnow.motives

import org.junit.runner.RunWith
import org.specs2.mutable._
import org.specs2.runner._

import scalax.collection.Graph
import scalax.collection.GraphPredef._

/**
 * Created by daveshepard on 10/2/15.
 */
object MockNode {
  var cache = Map[Int, MockNode]()

  def apply(value:Int, label:String) = if (cache.contains(value)) cache(value) else {
    val item = new MockNode(value, label)
    cache = cache.updated(value, item)
    item
  }
}

class MockNode(val value:Int, val label:String) extends LabelledNode {

  override def toString = s"(${value}, ${label})"

  override def equals(o:Any) = {
    val n = o.asInstanceOf[MockNode]

    n.value == value && n.label == label
  }

}


@RunWith(classOf[JUnitRunner])
class MockNodeSpec extends Specification {

  "A MockNode should" >> {

    "be equal to itself" >> {
      val n1 = MockNode(1, "red")
      val n2 = MockNode(2, "blue")

      val list = List(n1, n2)

      list must contain(n1)
      list must contain(n2)
    }

    "be equal to itself when added to a graph" >> {

      val n1 = MockNode(1, "red")
      val n2 = MockNode(2, "blue")

      val list = List(n1, n2)

      val graph = Graph(n1~n2)

      graph.edges.foreach(e => {
        list must contain(e._1.value)
        list must contain(e._2.value)
      })

      list must haveSize(2)

    }

  }

}