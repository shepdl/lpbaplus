package edu.ucla.hcnow.motives

import org.junit.runner.RunWith
import org.specs2.mutable._
import org.specs2.runner._

import scalax.collection.GraphEdge.UnDiEdge
import scalax.collection.edge.WUnDiEdge


/**
 * Created by daveshepard on 10/22/15.
 */
@RunWith(classOf[JUnitRunner])
class FindCommunitiesToMergeSpec extends Specification with GraphTestCase3 {

  val instance = new BipartiteCommunityList[MockNode, WUnDiEdge](graph3, red, blue)

  "The merge communities function " >> {

    "merge communities 1 and 2 in test case 3" >> {

      val initialModularity = instance.bipartiteModularityForGraph(graph3, communityList)

      val merges = instance.findCommunitiesToMerge(graph3, communityList)
      println(merges)
      merges must contain((1,2))
      merges must contain((3,4))
      merges must not contain(1,5)
      merges must not contain(1,3)
      merges must not contain(1,4)

    }
  }

}
