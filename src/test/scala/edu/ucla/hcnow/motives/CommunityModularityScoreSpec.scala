package edu.ucla.hcnow.motives

import org.junit.runner.RunWith
import org.specs2.mutable._
import org.specs2.runner._

import scalax.collection.Graph
import scalax.collection.GraphEdge.UnDiEdge
import scalax.collection.edge.WUnDiEdge


/**
 * Specification for Community Modularity Score function
 *
 * Created by daveshepard on 10/11/15.
 */
@RunWith(classOf[JUnitRunner])
class CommunityModularityScoreSpec extends Specification with GraphLabels {

  val instance = new BipartiteCommunityList[MockNode, WUnDiEdge](null, red, blue)

  "The Community Modularity Score for each node function" >> {

    "return something close to 1.6923 in case one" >> {
      instance.communityModularityScoreForNodes(4, 3, 10, 13) must beCloseTo(1.6923, tolerance)
    }

    "return something close to -0.5 in case two" >> {
      instance.communityModularityScoreForNodes(10, 14, 30, 40) must beCloseTo(-0.5, tolerance)
    }

    "Return something close to 39.0909 in case three" >> {
      instance.communityModularityScoreForNodes(50, 40, 15, 55) must beCloseTo(39.090909, tolerance)
    }

  }

}
