package edu.ucla.hcnow.motives

import org.junit.runner.RunWith
import org.specs2.mutable._
import org.specs2.runner._

import scalax.collection.Graph
import scalax.collection.GraphEdge.UnDiEdge
import scalax.collection.GraphPredef._
import scalax.collection.edge.WUnDiEdge


/**
 * Created by daveshepard on 10/7/15.
 */
@RunWith(classOf[JUnitRunner])
class ModularityContributionOfCommunitySpec extends Specification with GraphTestCase1 {

  val instance = new BipartiteCommunityList[MockNode, WUnDiEdge](graph, red, blue)

  "In test case 1, " >> {


    "the Modularity Contribution of a community should " >> {

      "be near 0.1632 for community 1" >> {
        val contribution = instance.modularityContributionOfCommunity(graph, communities.communitiesToNodes(1))
        contribution must beCloseTo(0.1632, tolerance)
      }

      "be near 0.1632 for community 2" >> {
        val contribution = instance.modularityContributionOfCommunity(graph, communities.communitiesToNodes(2))
        contribution must beCloseTo(0.1632, tolerance)
      }

    }
  }

}

@RunWith(classOf[JUnitRunner])
class ModularityContributionOfCommunitySpecTestCase2 extends Specification with GraphTestCase2 {

  val instance = new BipartiteCommunityList[MockNode, WUnDiEdge](graph2, red, blue)

  "In test case 2:" >> {

    "the modularity contribution of a community should" >> {

      "be near 0.166666 for community 1" >> {
        val contribution = instance.modularityContributionOfCommunity(graph2, communities2.communitiesToNodes(1))
        contribution must beCloseTo(0.166666, tolerance)
      }

      "be near 0.111111 for community 2" >> {
        val contribution = instance.modularityContributionOfCommunity(graph2, communities2.communitiesToNodes(2))
        contribution must beCloseTo(0.111111, tolerance)
      }

      "be near 0.208333 for community 3" >> {
        val contribution = instance.modularityContributionOfCommunity(graph2, communities2.communitiesToNodes(3))
        contribution must beCloseTo(0.208333, tolerance)
      }
    }
  }

}
