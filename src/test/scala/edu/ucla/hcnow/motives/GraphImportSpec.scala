package edu.ucla.hcnow.motives

import org.junit.runner.RunWith
import org.specs2.mutable._
import org.specs2.runner._

import scalax.collection.GraphEdge.UnDiEdge
import scalax.collection.GraphPredef._



/**
 * Created by daveshepard on 10/22/15.
 */
@RunWith(classOf[JUnitRunner])
class GraphImportSpec extends Specification {

  "The Graph Import feature " >> {

    val filename = "../../src/test/scala/edu/ucla/hcnow/motives/testCase4.csv"

    "finds all nodes in test case 4" >> {
      val graph = CSVImporter.graphFromEdgeList(filename, "word", "author", "weight")

      graph.contains(StringLabelledNode("w1", "red")) must beTrue
      graph.contains(StringLabelledNode("w2", "red")) must beTrue
      graph.contains(StringLabelledNode("w3", "red")) must beTrue
      graph.contains(StringLabelledNode("w4", "red")) must beTrue

      graph.contains(StringLabelledNode("a1", "blue")) must beTrue
      graph.contains(StringLabelledNode("a2", "blue")) must beTrue
      graph.contains(StringLabelledNode("a3", "blue")) must beTrue
      graph.contains(StringLabelledNode("a5", "blue")) must beTrue

      graph.contains(StringLabelledNode("b12", "blue")) must beFalse
    }

  }

}
