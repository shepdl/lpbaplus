package edu.ucla.hcnow.motives

import org.junit.runner.RunWith
import org.specs2.mutable._
import org.specs2.runner._

import scalax.collection.GraphEdge.UnDiEdge
import scalax.collection.edge.WUnDiEdge


/**
 * Created by daveshepard on 10/8/15.
 */
@RunWith(classOf[JUnitRunner])
class ModularityContributionsSpec extends Specification with GraphTestCase1 with GraphTestCase2 {

  val instance = new BipartiteCommunityList[MockNode, WUnDiEdge](graph2, red, blue)

  "The modularity contributions function should" >> {

    "return a list of communities and their modularity contributions when supplied a graph and list of nodes in graph 1" >> {
      val modularityContributions = instance.modularityContributions(graph, communities)
      modularityContributions(1) must beCloseTo(0.1632, tolerance)
      modularityContributions(2) must beCloseTo(0.1632, tolerance)
    }

    "return a list of communities and their modularity contributions when supplied a graph and list of nodes in graph 2" >> {
      val modularityContributions = instance.modularityContributions(graph2, communities2)
      modularityContributions(1) must beCloseTo(instance.modularityContributionOfCommunity(graph2, communities2.communitiesToNodes(1)), tolerance)
      modularityContributions(2) must beCloseTo(instance.modularityContributionOfCommunity(graph2, communities2.communitiesToNodes(2)), tolerance)
      modularityContributions(3) must beCloseTo(instance.modularityContributionOfCommunity(graph2, communities2.communitiesToNodes(3)), tolerance)
    }

  }

}
