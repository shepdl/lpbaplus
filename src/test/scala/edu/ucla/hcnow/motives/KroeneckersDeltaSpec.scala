package edu.ucla.hcnow.motives

import org.junit.runner.RunWith
import org.specs2.mutable._
import org.specs2.runner._

import scalax.collection.Graph
import scalax.collection.edge.Implicits._
import scalax.collection.GraphEdge.UnDiEdge
import scalax.collection.GraphPredef._


/**
 * Created by daveshepard on 10/1/15.
 */
@RunWith(classOf[JUnitRunner])
class KroeneckersDeltaSpec extends Specification {

  "The Kroenecker's Delta function " >> {

    "must return 1 if values are equal" >> {
      val instance = new BipartiteCommunityList(null, "red", "blue")

      instance kroneckerDelta(1, 1) must_== 1
      instance kroneckerDelta(2, 2) must_== 1
      instance kroneckerDelta(100, 100) must_== 1
      instance kroneckerDelta(-100, -100) must_== 1
    }

    "must return 0 if the values are not equal" >> {
      val instance = new BipartiteCommunityList(null, "red", "blue")

      instance kroneckerDelta(0, 1) must_== 0
      instance kroneckerDelta(1, 0) must_== 0
      instance kroneckerDelta(100, 101) must_== 0
    }
  }

}



