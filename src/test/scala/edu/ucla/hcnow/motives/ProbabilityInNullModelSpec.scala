package edu.ucla.hcnow.motives

import org.junit.runner.RunWith
import org.specs2.mutable._
import org.specs2.runner._


/**
 * Created by daveshepard on 10/1/15.
 */
@RunWith(classOf[JUnitRunner])
class ProbabilityInNullModelSpec extends Specification with GraphTestCase1 {

  "The ~P function must" >> {
    "return the red degree times the blue degree divided by the number of edges in the graph" >> {

      val instance = new BipartiteCommunityList(graph, "red", "blue")
      instance.probabilityInNullModel(10, 10, 5) must_== 20
      instance.probabilityInNullModel(0, 1, 10) must_== 0
      instance.probabilityInNullModel(10, 20, 1) must_== 200

    }

    "return a decimal value when appropriate" >> {
      val instance = new BipartiteCommunityList(graph, "red", "blue")

      instance.probabilityInNullModel(1, 3, 2) must_== 1.5
    }
  }

}
