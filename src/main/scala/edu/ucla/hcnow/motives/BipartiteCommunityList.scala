package edu.ucla.hcnow.motives

import java.util.Calendar

import com.github.tototoshi.csv.CSVReader
import com.typesafe.scalalogging.Logger
import org.slf4j.LoggerFactory

import scala.collection.parallel.ForkJoinTaskSupport
import scala.concurrent.forkjoin.ForkJoinPool
import scalax.collection.Graph
import scalax.collection.GraphPredef._
import scalax.collection.edge.Implicits._
import scalax.collection.edge.WUnDiEdge

/**
 * Interface: BipartiteCommunityList object, with from(Graph:graph) method
 * Other objects are private
 *
 */
object BipartiteCommunityList {

  def from[N <: LabelledNode, E[X] <: WUnDiEdge[X]](graph:Graph[N, E], redLabel:String, blueLabel:String) = {
    val communityList = new BipartiteCommunityList[N, E](graph, redLabel, blueLabel)
    communityList.LPAbPlus(graph)
  }

}

class BipartiteCommunityList[N <: LabelledNode, E[X] <: WUnDiEdge[X]](graph:Graph[N,E], redLabel:String, blueLabel:String) {

  val logger = Logger(LoggerFactory.getLogger("Bipartite Community List Algorithm"))

  info(s"Initializing algorithm ...")

  // val nodeCount = graph.nodes.size.toDouble
  private val redNodes = graph.nodes.filter(_.label == redLabel).par
  redNodes.tasksupport = new ForkJoinTaskSupport(new ForkJoinPool(8))
  private val blueNodes = graph.nodes.filter(_.label == blueLabel).par
  blueNodes.tasksupport = new ForkJoinTaskSupport(new ForkJoinPool(8))

  info(s"Found ${redNodes.size} red nodes, ${blueNodes.size} blue nodes, and ${graph.edges.size} edges.")
  // val edgeCount = graph.edges.size.toDouble
  val edgeDegreeSum = graph.edges.map(_.weight).sum

  def weightedDegree(node:N) = graph.get(node).edges.map(_.weight).sum
  def degrees(node:N) = weightedDegree(node)


  def LPAbPlus(graph:Graph[N, E]):CommunityList[N] = {
    info("Beginning LPAb+ calculation ...")

    val (initialAssignments, _) = graph.nodes.foldLeft(Pair(new CommunityList[N](), 0)) {
      case ((acc, index), node) => (acc.updated(node, index), index + 1)
    }
    info("Initial assignments complete")

    val newCommunities = updateLabels(graph, initialAssignments, bipartiteModularityForGraph(graph, initialAssignments))
    val communitiesWithPositiveModularityChanges = communityMergesCreatingHighestModularityIncrease(graph, newCommunities)

    /*
    info("Calculating modularity contributions before")
    val beforeModularityContributions = modularityContributions(graph, newCommunities)
    // The division step--pruning
    val newCommunities2 = newCommunities.prune
    info("Calculating modularity contributions after")
    val afterModularityContributions = modularityContributions(graph, newCommunities2)

    val communitiesWithPositiveModularityChanges = beforeModularityContributions.zip(afterModularityContributions).filter({
      case ((index, before), (_, after)) => {
        println(s"Comm ${index}: ${before} - ${after} = ${before - after}")
        // TODO: Double-check this
        before - after > 0
      }
    }).map({
      case ((index, before), (_, after)) => before - after
    })
    */

    if (communitiesWithPositiveModularityChanges.nonEmpty) {
      info("Found communities with positive modularity changes")
//      mergeCommunityPairs(graph, newCommunities2)
        mergeCommunityPairs(graph, newCommunities)
    } else {
      newCommunities
    }

  }

  /**
   * Find community inducing the highest positive modularity change when merged with the existing community
   *
   * Returns -1 if there is no such community
   *
   * @param graph
   * @param communityList
   * @param community1
   * @return
   */
  def communityInducingHighestModularityChange(graph:Graph[N, E], communityList:CommunityList[N], community1:Int) = {
    val currentModularity = bipartiteModularityForGraph(graph, communityList)
    communityList.communities.map(comparison => {
      (comparison, currentModularity - bipartiteModularityForGraph(graph, communityList.mergeCommunities(community1, comparison)))
    }).maxBy {
      case (index, newModularity) => newModularity
    } match {
      case (index, modularity) => if (modularity > 0) index else -1
    }
  }

  def communityMergesCreatingHighestModularityIncrease(graph:Graph[N, E], communityList: CommunityList[N]) = {
    val currentModularity = bipartiteModularityForGraph(graph, communityList)
    val finalCommunities = communityList.combinations.par.map {
      case (community1, community2) => ((community1, community2), bipartiteModularityForGraph(graph, communityList.mergeCommunities(community1, community2)) - currentModularity )
    }.filter {
      // We only want positive modularity changes
      case (pair, modularity) => modularity > 0
    }.seq.toList.sortBy {
      case (pair, newModChange) => newModChange
    }.foldLeft(Map[Int, Int]()) {
      case (acc, ((c1, c2), _)) => if (acc.contains(c1) || acc.contains(c2)) acc
      else {
        acc.updated(c1, c2).updated(c2, c1)
      }
    }

    // Eliminate backwards binding in map of community pairs to remove
    finalCommunities.values.foldLeft(finalCommunities) {
      case (acc, comm) => if (acc.contains(comm) && acc.contains(acc(comm))) acc - comm else acc
    }
  }

  def findCommunitiesToMerge(graph:Graph[N, E], communityList:CommunityList[N]) = {
    val merges = communityList.combinations.par.filter {
      case (community1, community2) =>  communityInducingHighestModularityChange(graph, communityList, community1) == community2 &&
        communityInducingHighestModularityChange(graph, communityList, community2) == community1
    }.foldLeft(List[(Int, Int)]()) {
      case (acc, pair) => if (acc.contains(pair)) acc else pair :: acc
    }
    merges
  }

  def mergeCommunityPairs(graph:Graph[N, E], communityList:CommunityList[N]):CommunityList[N] = {
    info("Merging community pairs")

    val newCommunityList = findCommunitiesToMerge(graph, communityList).foldLeft(communityList) {
      case (newComm, (comm1, comm2)) => newComm.mergeCommunities(comm1, comm2)
    }

    val newCommunities = updateLabels(graph, newCommunityList, bipartiteModularityForGraph(graph, newCommunityList))
    val beforeModularityContributions = modularityContributions(graph, newCommunities)
    val newCommunities2 = newCommunities.prune
    val afterModularityContributions = modularityContributions(graph, newCommunities2)

    val communitiesWithPositiveModularityChanges = beforeModularityContributions.zip(afterModularityContributions).filter({
      case ((index, before), (_, after)) => before - after > 0
    }).map({
      case ((index, before), (_, after)) => before - after
    })

    if (communitiesWithPositiveModularityChanges.nonEmpty) {
      mergeCommunityPairs(graph, newCommunities2)
    } else {
      // TODO: newCommunities or newCommunities2?
      newCommunities
    }
  }


  def modularityContributions(graph:Graph[N, E], communityList:CommunityList[N]) = {
    communityList.communitiesToNodes.foldLeft(Map[Int, Double]()) {
      case (acc, (communityIndex, nodes)) => acc.updated(communityIndex, modularityContributionOfCommunity(graph, nodes))
    }
  }

  def modularityContributionOfCommunity(graph:Graph[N, E], community:List[N]):Double = {
    val totalWeightInCommunity = community.map(node => {
        graph.get(node).edges.filter(e => community.contains(e._1.value) && community.contains(e._2.value))
          .map(_.weight).sum
      }).sum / 2.0
    val redNodesTotalDegree = community.filter(_.label == redLabel).map(degrees(_)).sum.toDouble
    val blueNodesTotalDegree = community.filter(_.label == blueLabel).map(degrees(_)).sum.toDouble

    (totalWeightInCommunity / edgeDegreeSum) - ((redNodesTotalDegree * blueNodesTotalDegree) / Math.pow(edgeDegreeSum, 2))
  }

  def probabilityInNullModel(redNodeDegree:Double, blueNodeDegree:Double, numEdges:Double) = redNodeDegree * blueNodeDegree / numEdges


  def info(message:String) = logger.info(Calendar.getInstance.getTime.toString + " " + message)

  def bipartiteModularityForGraph(graph:Graph[N, E], communityList: CommunityList[N]):Double = {
    (1.0 / edgeDegreeSum) * communityList.communitiesToNodes.values.par.map(nodes => {
      val reds = nodes.filter(_.label == redLabel).map(graph.get(_))
      val blues = nodes.filter(_.label == blueLabel).map(graph.get(_))
      reds.map(r => blues.map(b => {
        r.connectionsWith(b).map(_.weight).foldLeft(0.0)(_ + _) - weightedDegree(r.value) * weightedDegree(b.value) / edgeDegreeSum
      }).sum).sum
    }).sum
  }


  def kroneckerDelta(i:Int, j:Int) = if (i == j) 1 else 0


  def communityModularityScoreForNodes(neighborCount:Double, degreeOfThisTypeOfNode:Double,
                                       totalDegreeOfOtherTypeOfNodesInCommunity:Double, totalEdges:Double) =
    neighborCount - ((degreeOfThisTypeOfNode * totalDegreeOfOtherTypeOfNodesInCommunity) / totalEdges)

  def updateCommunityList(communities:CommunityList[N], otherNodesInCommunitiesDegrees:Map[Int, Double], n:Graph[N,E]#NodeT) = {

    val nodeDegree = degrees(n.value)
    val (newIndex, _) = n.neighbors.map(n => communities.communityForNode(n.value)).map({
      case communityIndex => (communityIndex, communityModularityScoreForNodes(
        n.neighbors.filter(n => communities.communityForNode(n.value) == communityIndex)
          .map(n => degrees(n.value)).sum,
//        degrees(n.value),
        nodeDegree,
        otherNodesInCommunitiesDegrees(communityIndex),
        edgeDegreeSum
      ))
    }).maxBy({
      case (index, modularity) => modularity
    })
    (n.value, newIndex)
  }

  /**
   * LPAb' (Algorithm 1)
   *
   * Implementation of LPBA' component of algorithm.
   *
   *
   * @param graph Starting graph
   * @param communities
   * @param initialModularity
   * @return
   */
  private def updateLabels(graph:Graph[N, E], communities:CommunityList[N], initialModularity:Double):CommunityList[N] = {
    info("Updating labels")

    info("Updating red nodes ...")
    val blueNodeCommunityDegrees = communities.communitiesToNodes.foldLeft(Map[Int, Double]()) {
      case (acc, (key, nodes)) => acc.updated(key, nodes.filter(_.label == blueLabel).map(degrees(_)).sum)
    }
    info("Calculated degrees for red node computation")
    val redNodeAssignments = redNodes.map(n => updateCommunityList(communities, blueNodeCommunityDegrees, n))
    info("Updating blue nodes")
    val redNodeCommunityDegrees = communities.communitiesToNodes.foldLeft(Map[Int, Double]()) {
      case (acc, (key, nodes)) => acc.updated(key, nodes.filter(_.label == redLabel).map(degrees(_)).sum)
    }
    info("Calculated degrees for blue node computation")
    val blueNodeAssignments = blueNodes.map(n => updateCommunityList(communities, redNodeCommunityDegrees, n))
    info("Assignments complete; updating community list ...")

    val withBlueAndRedNodes = (blueNodeAssignments ++ redNodeAssignments).foldLeft(new CommunityList[N]()) {
      case (comm, (node, index)) => comm.updated(node, index)
    }
    info("Completed updating community list")

    val newModularity = bipartiteModularityForGraph(graph, withBlueAndRedNodes)
    if (newModularity <= initialModularity) {
      // return new community list
      withBlueAndRedNodes
    } else {
      updateLabels(graph, withBlueAndRedNodes, newModularity)
    }
  }

  info("Algorithm initialization complete")

}
