package edu.ucla.hcnow.motives

/**
 * Created by daveshepard on 10/1/15.
 */
trait LabelledNode {
  val label:String
}
