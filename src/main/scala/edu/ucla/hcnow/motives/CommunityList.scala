package edu.ucla.hcnow.motives

/**
 * Created by daveshepard on 9/22/15.
 */

class CommunityList[A](val communitiesToNodes:Map[Int, List[A]], nodesToCommunities:Map[A, Int]) {

  def this(commsToNodes:Map[Int, List[A]]) = this(commsToNodes, commsToNodes.foldLeft(Map[A,Int]()) {
      case (acc, (community, nodes)) => nodes.foldLeft(acc) {
        case (acc, node) => acc.updated(node, community)
      }
    })

  def this() = this(Map[Int, List[A]](), Map[A, Int]())

  def communityForNode(node:A) = nodesToCommunities.getOrElse(node, -1)

  def updated(node:A, communityIndex:Int) = {
    val newNodeList = if (communitiesToNodes.contains(communityIndex)) {
      node :: communitiesToNodes(communityIndex).filterNot(_ == node)
    } else {
      node :: List()
    }
    val newCommunitiesToNodes = communitiesToNodes.updated(communityIndex, newNodeList)
    val newNodesToCommunities = nodesToCommunities.updated(node, communityIndex)

    new CommunityList[A](newCommunitiesToNodes, newNodesToCommunities)
  }

  def prune = new CommunityList(communitiesToNodes.foldLeft(Map[Int, List[A]]())({
      case (acc, (index, items)) => if (items.isEmpty) acc else acc.updated(index, items)
    }), nodesToCommunities)

  def combinations = communitiesToNodes.keys.zip(communitiesToNodes.keys.tail).filter({
    case (community1, community2) => community1 != community2
  })

  def nodeCombinations = nodesToCommunities.keys.zip(nodesToCommunities.keys.tail).filter({
    case (community1, community2) => community1 != community2
  })

  def mergeCommunities(index1:Int, index2:Int) = {
    val mergedNodes = communitiesToNodes(index1) ++ communitiesToNodes(index2)
    val communitiesToNodesWithoutIndex2 = communitiesToNodes - index2
    val newNodesToCommunities = nodesToCommunities.foldLeft(Map[A, Int]()) {
      case (acc, (node, communityIndex)) => {
        val newIndex = if (communityIndex == index2) { index1 } else { communityIndex }
        acc.updated(node, newIndex)
      }
    }

    new CommunityList[A](communitiesToNodes.updated(index1, mergedNodes), newNodesToCommunities)
  }

  def communities = communitiesToNodes.keys

}
