package edu.ucla.hcnow.motives

import com.github.tototoshi.csv.CSVReader
import java.io.File

import scalax.collection.mutable.Graph
import scalax.collection.GraphPredef._
import scalax.collection.edge.Implicits._
import scalax.collection.edge.WUnDiEdge

/**
 * Created by daveshepard on 10/23/15.
 */
object CSVImporter {

  def graphFromEdgeList[N <: LabelledNode, E[X] <: WUnDiEdge[X]](filename:String, redHeading:String, blueHeading:String, weightHeading:String) = {
    CSVReader.open(new File(filename)).iteratorWithHeaders.foldLeft(Graph[StringLabelledNodeImpl, WUnDiEdge]()) {
      case (graph, edge) => {
        val red = StringLabelledNode(edge(redHeading), redHeading)
        val blueRaw = edge(blueHeading)
        /*
        val blueText = blueRaw.substring(2, blueRaw.length - 1)
                .replaceAll("""[\p{Punct}&&[^#']]""", "")
                .replaceAll("""\d+""", "")
                .toLowerCase()
        val blue = StringLabelledNode(blueText, blueHeading)


        if (blueText != "" && !blueRaw.contains("\\x")) {
        */
        if (edge(weightHeading).toLong > 1) {
          val blue = StringLabelledNode(blueRaw, blueHeading)
          graph += red ~ blue % edge(weightHeading).toLong
        } else {
          graph
        }
      }
    }
  }

}

case class StringLabelledNodeImpl(key:String, val label:String) extends LabelledNode {
  override def toString = s"${label} - ${key}"
}

case object StringLabelledNode {
  var cache = Map[String, StringLabelledNodeImpl]()
  def apply(key:String, label:String) = {
    if (cache.contains(key)) {
      cache(key)
    } else {
      val node = StringLabelledNodeImpl(key, label)
      cache = cache.updated(key, node)
      node
    }
  }
}

