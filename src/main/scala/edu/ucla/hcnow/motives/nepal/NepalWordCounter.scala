package edu.ucla.hcnow.motives.nepal

import java.io.{FileWriter, BufferedWriter}

import com.typesafe.scalalogging.Logger
import edu.ucla.hcnow.motives.{BipartiteCommunityList, CSVImporter}
import org.slf4j.LoggerFactory
import java.util.Date

/**
 * Created by daveshepard on 10/24/15.
 */
object NepalWordCounter extends App {

  override def main (args: Array[String]) = {
    val logger = Logger(LoggerFactory.getLogger("NepalWordCounter"))

    // load author-word graph
//    val filename = "/Users/daveshepard/Documents/Research and Projects/Jishin/Nepal/author-word-vectors/author_word_vectors_filtered_2.csv"
    val filename = "/Users/daveshepard/Documents/Research and Projects/Jishin/Nepal/author-word-vectors/author_word_vectors.sample.csv"
    logger.info(s"Loading data from ${filename} ...")
    val graph = CSVImporter.graphFromEdgeList(filename, "user", "word", "weight")
    logger.info("Data loaded ...")
    // generate community list
    logger.info("Calculating modularity ...")
    val communityList = BipartiteCommunityList.from(graph, "user", "word")
    // write out list of communities to file
    logger.info("Writing out ...")
    val outFilename = s"output-${new Date().toString}.txt"
    logger.info(s"Writing to ${outFilename}")
    val writer = new BufferedWriter(new FileWriter(outFilename))
    communityList.communitiesToNodes.foreach({
      case (commId, nodes) => {
//        println(s"${commId} ->")
        writer.write(s"${commId} ->\n")
//        nodes.foreach(node => println(s"\t${node}"))
        nodes.foreach(node => writer.write(s"\t${node}\n"))
//        print("\n\n")
        writer.write("\n\n")
      }
    })
    writer.close()
  }
}
