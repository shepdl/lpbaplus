name := "LPAbPlus"

version := "1.0"

scalaVersion := "2.11.7"


libraryDependencies += "com.assembla.scala-incubator" %% "graph-core" % "1.9.4"

libraryDependencies ++= Seq(
  "org.specs2" %% "specs2-core" % "3.6.4" % "test",
  "org.specs2" %% "specs2-junit" % "3.6.4" % "test"
)

libraryDependencies += "com.github.tototoshi" %% "scala-csv" % "1.2.2"

libraryDependencies += "com.typesafe.scala-logging" %% "scala-logging" % "3.1.0"

libraryDependencies += "org.slf4j" % "slf4j-simple" % "1.7.12"


scalacOptions in Test ++= Seq("-Yrangepos")

test in assembly := {}
